package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState initialState;
    CellState[][] grid = new CellState[0][0];


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        this.grid = new CellState[rows][columns];

        for (int row = 0; row < rows; row++){
            for (int column = 0; column < columns; column++){
                grid[row][column] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid rObj = new CellGrid(rows, columns, initialState);

        for (int row = 0; row < rows; row++){
            for (int column = 0; column < columns; column++){
                rObj.set(row, column, grid[row][column]);
            }
        }
        return rObj;
    }
}
